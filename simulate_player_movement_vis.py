import cv2 as cv
import math

import numpy as np
from PIL import Image

if __name__ == '__main__':
    skull_icon = Image.open("skull_icon.png")
    r, g, b, a = skull_icon.split()
    skull_icon = Image.merge("RGB", (r, g, b))
    skull_icon_mask = Image.merge("L", (a,))

    red_icon = Image.open("red_icon.png")
    r, g, b, a = red_icon.split()
    red_icon = Image.merge("RGB", (r, g, b))
    red_icon_mask = Image.merge("L", (a,))

    # Define the codec and create VideoWriter object
    fourcc = cv.VideoWriter_fourcc(*'H264')
    out = cv.VideoWriter('output-g01.mkv', fourcc, 30.0, (1500, 1500))
    map = cv.imread("map_no_border.png")
    players = []
    maximum = 0
    for idx in range(0, 100):
        with open('movements.g01/movement_p{:d}.csv'.format(idx)) as f:
            player_temp = f.readlines()
        player_temp = [x.strip() for x in player_temp]
        players.append(player_temp)
        maximum = max(len(player_temp), maximum)

    for line in range(0, maximum+1):
        img = cv.cvtColor(map.copy(), cv.COLOR_BGR2RGB)
        pil_im = Image.fromarray(img).convert('RGBA')

        for player_temp in players:
            if len(player_temp) > line:
                i, x, y = player_temp[line].split()
                # pil_im.paste(skull_icon, (int(float(x)), int(float(y))), skull_icon_mask)
                pil_im.paste(red_icon, (int(float(x)), int(float(y))), red_icon_mask)

                # cv.circle(img, (int(float(x)), int(float(y))), 5, 255, -1)
            else :
                i, x, y = player_temp[-1].split()
                pil_im.paste(skull_icon, (int(float(x)), int(float(y))), skull_icon_mask)
        out.write(cv.cvtColor(np.array(pil_im.convert('RGB')), cv.COLOR_RGB2BGR))
    out.release()