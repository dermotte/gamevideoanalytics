import re

import cv2 as cv
import numpy as np
import pyocr.builders
from PIL import Image


def extractHOG_small(img):
    bin_n = 32  # Number of bins
    my_img = cv2.resize(img, (20, 20))
    gx = cv.Sobel(my_img, cv.CV_32F, 1, 0)
    gy = cv.Sobel(my_img, cv.CV_32F, 0, 1)
    mag, ang = cv.cartToPolar(gx, gy)

    # quantizing binvalues in (0...16)
    bins = np.int32(bin_n * ang / (2 * np.pi))

    # Divide to 4 sub-squares
    bin_cells = bins[:10, :10], bins[10:, :10], bins[:10, 10:], bins[10:, 10:]
    mag_cells = mag[:10, :10], mag[10:, :10], mag[:10, 10:], mag[10:, 10:]
    hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]
    hist = np.hstack(hists)
    eps = 1e-12
    hist /= hist.sum() + eps  # normalize hist
    return hist


def clamp(minimum, maximum, val):
    return max(minimum, min(val, maximum))


def submatrix(matrix, p1=(1000, 190), p2=(1200, 260)):
    # x1 = clamp(0, 1280, point[0] - distance)
    # x2 = clamp(0, 1280, point[0] + distance)
    # y1 = clamp(0, 720 - 1, point[1] - distance)
    # y2 = clamp(0, 720, point[1] + distance)
    return matrix[p1[1]:p2[1], p1[0]:p2[0]], p1[0], p1[1]


def find_best_match(region, phase_icons, mask_icon):
    result = []
    max_values = []
    max_locations = []
    for icon_idx in range(0, len(phase_icons)):
        result.append(cv.matchTemplate(region, phase_icons[icon_idx], cv.TM_CCORR_NORMED, mask=mask_icon))
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(result[-1])
        max_values.append(max_val)
        max_locations.append(max_loc)
    max_idx = np.argmax(max_values)
    return max_idx, max_values[max_idx], max_locations[max_idx]


if __name__ == '__main__':
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    tools = pyocr.get_available_tools()
    if len(tools) == 0:
        print("No OCR tool found")
    # The tools are returned in the recommended order of usage
    tool = tools[0]
    print("Will use tool '%s'" % (tool.get_name()))
    # Ex: Will use tool 'libtesseract'

    langs = tool.get_available_languages()
    lang = 'eng'
    builder = pyocr.tesseract.DigitBuilder()

    # 0 - searching
    # 1 - bus
    # 2 - jump
    # 3 - storm
    # 4 - contract
    phases = [0, 1, 2, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4]
    mask_icon = cv.imread("mask_icon.png")
    person_icon_mask = cv.imread("person_icon_mask.png")
    person_icon = cv.imread("person_icon.png")

    # cap = cv.VideoCapture("done/fortnite_3JfkjN-3nto.mp4")
    cap = cv.VideoCapture("done/fortnite_GS4Qjrb0-74.mp4")
    # cap = cv.VideoCapture("done/fortnite_pOeyoa9a-hg.mp4")
    # cap = cv.VideoCapture("done/fortnite_Ki70pq3w0CI.mp4")

    phase_icons = [cv.imread("bus_icon.png", cv.IMREAD_COLOR),
                   cv.imread("jump_icon.png", cv.IMREAD_COLOR), cv.imread("storm_icon.png", cv.IMREAD_COLOR),
                   cv.imread("contract_icon.png", cv.IMREAD_COLOR)]

    phase_idx = 0
    # position fixed ...
    lastPosition = (0, 0)
    samePositionCount = 0
    lowValueCount = 0
    frameCount = 0
    isInPause = False
    playerCountHistory = ['100']

    # skip ahead some frames
    for c in range(0, 250):
        ret, frame = cap.read()
        frameCount += 1

    while 1:
        ret, frame = cap.read()

        if samePositionCount < 30:
            region, x1, y1 = submatrix(frame)
        else:
            region, x1, y1 = submatrix(frame, lastPosition)

        # res = cv.matchTemplate(region, phase_icons[phases[phase_idx]], cv.TM_CCORR_NORMED, mask = mask_icon)
        # min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        current_phase, max_val, max_loc = find_best_match(region, phase_icons, mask_icon)

        if lastPosition == (max_loc[0] + x1, max_loc[1] + y1) and max_val > 0.95:
            samePositionCount += 1
        else:
            samePositionCount = 0

        lastPosition = (max_loc[0] + x1, max_loc[1] + y1)
        frameCount += 1
        top_left = (max_loc[0], max_loc[1])

        # here is the text to recognize:
        count_players = '??'
        if samePositionCount < 30:
            current_phase = -1
        text_img = None
        # if samePositionCount > 10 and frameCount % 30 == 0:
        if True:  # frameCount % 30 == 0:
            res = cv.matchTemplate(frame[lastPosition[1]:lastPosition[1] + 25, lastPosition[0]:1270], person_icon,
                                   cv.TM_CCORR_NORMED, mask=person_icon_mask)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)

            text_img = frame[max_loc[1] + lastPosition[1]:max_loc[1] + lastPosition[1] + 20,
                       max_loc[0] + lastPosition[0] + 23:max_loc[0] + lastPosition[0] + 45]
            text_img = cv.cvtColor(text_img, cv.COLOR_BGR2GRAY)
            text_img = cv.bilateralFilter(text_img, 5, 100, 100)
            text_img = cv.equalizeHist(text_img)
            text_img = cv.threshold(text_img, 200, 255, cv.THRESH_BINARY_INV)[1]

            pil_im = Image.fromarray(text_img)
            count_players = tool.image_to_string(
                pil_im,
                lang=lang,
                builder=builder
            ).strip()
            pattern = re.compile('[1-9][0-9]?')
            if not pattern.match(count_players) or len(count_players) > 2:
                count_players = "??"
            else:
                if int(count_players) < int(playerCountHistory[-1]) - 20: # it's unrealistic that more than XYZ die in one frame ...
                     count_players = '??'
            if count_players != '??':
                playerCountHistory.append(count_players)
            k = 30
            if len(playerCountHistory) > k:  # majority vote ..
                voter = {}
                for i in range(0, k):
                    if playerCountHistory[-1 - i] not in voter and playerCountHistory[-1 - i] != '??':
                        voter[playerCountHistory[-1 - i]] = 1
                    else:
                        voter[playerCountHistory[-1 - i]] += 1
                count_players = sorted(voter, key=voter.get)[0]
                # print(sorted(voter, key=voter.get), voter, playerCountHistory[-30:-1])
                # if count_players is not playerCountHistory[-2]:
                #     if not count_players == '??' and not playerCountHistory[-2] == '??':
                #         last_known_valid_number = count_players
        if frameCount > 1:
            if frameCount % 30 == 0:
                print("{:8d} {:2d} {:s} {:f} {:d}x{:d} ".format(frameCount, current_phase, count_players, max_val,
                                                                top_left[0] + x1,
                                                                top_left[1] + y1))
                # cv.rectangle(frame, (top_left[0] + x1, top_left[1] + y1),
                #              (top_left[0] + x1 + 25, top_left[1] + y1 + 25), 255, 3)
                # cv.imshow("video", frame)
                # if count_players != '??': cv.imshow("text", text_img)
                # cv.waitKey(3)
