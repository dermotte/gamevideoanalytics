"""
Simple OpenCV script for finding the player position in the fortnite map based on the minimaps.

we need:
  * map.png -> the 1500x1500 map of the island
  * map_mask.png -> the mask showing whats island and what is not, also in 1500x1500
  * the frames in 184x184 cut from a 720p video.

  for a video like this: https://www.youtube.com/watch?v=wnVWlChmxao
  $> youtube-dl -f "best[height>=720]" -o "fortnite_%(id)s.%(ext)s" https://www.youtube.com/watch?v=3JfkjN-3nto
  check with gimp where to crop and ...
  $> ffmpeg -i <720p>.mp4 -ss 00:00:01 -r 1 -filter:v "crop=185:185:1084:12" frames_wnVWlChmxao/frame_%d.png
"""
import cv2 as cv
import numpy as np
import time
import os.path


def clamp(minimum, maximum, val):
    return max(minimum, min(val, maximum))


def submatrix(res, p, n):
    x1 = clamp(0, len(res) - 1, p[0] - n)
    x2 = clamp(0, len(res) - 1, p[0] + n)
    y1 = clamp(0, len(res) - 1, p[1] - n)
    y2 = clamp(0, len(res) - 1, p[1] + n)
    return res[y1:y2, x1:x2], x1, y1

### Configure run here ...

videofiles = """
https://www.youtube.com/watch?v=9zqSV6xmBwY
https://www.youtube.com/watch?v=iKVmajpvrbA
https://www.youtube.com/watch?v=TW147vqcLAA
https://www.youtube.com/watch?v=9PtBkvuN4rM
https://www.youtube.com/watch?v=zy6sw6n_EqI
https://www.youtube.com/watch?v=H3VCrL_pc9c
https://www.youtube.com/watch?v=illGXEyTSfI
https://www.youtube.com/watch?v=2GUXW2V0aLs
https://www.youtube.com/watch?v=gOsIRUqgf9A
https://www.youtube.com/watch?v=pOeyoa9a-hg
https://www.youtube.com/watch?v=_yzKibn2I94
https://www.youtube.com/watch?v=Gfi9F7DYlws
https://www.youtube.com/watch?v=RFYw4a_uD9w
https://www.youtube.com/watch?v=wiAUzhXuU94
https://www.youtube.com/watch?v=Zprxg9VlctY
https://www.youtube.com/watch?v=Ki70pq3w0CI
""".replace("https://www.youtube.com/watch?v=", "").split()

visualization = True
id = videofiles[0]

if __name__ == '__main__':
    for idx in [15] :
        id = videofiles[idx]
        img = cv.imread('map.png', cv.IMREAD_GRAYSCALE)
        img_mask = cv.imread('map_mask.png', cv.IMREAD_GRAYSCALE)
        vis_color = cv.imread('map.png', cv.IMREAD_COLOR)
        clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        img = clahe.apply(img)

        heatmap = np.zeros(img_mask.shape)

        lastPositions = []

        outfile = open("out_"+id+".txt", "w")
        outfile.write("frame# x_pos y_pos omitFrame max_val distance " +
                      "mean_val_red_channel mean_val_green_channel mean_val_blue_channel " +
                      "mean_val_value_channel")

        start_time = time.time()

        for count in range(1, 1000000000):
            # read the green channel from the frame and omit those with a maximum less than 255:
            fname = 'done/frames_' + id + '/frame_{:d}.png'.format(count)
            ## exit if this file does not exist.
            if not os.path.isfile(fname): break
            f = cv.imread(fname, cv.IMREAD_COLOR)
            frame_green = f[:, :, 1]
            frame_blue = f[:, :, 0]
            frame_red = f[:, :, 2]


            frame = cv.imread(fname, cv.IMREAD_GRAYSCALE)
            lightness = np.mean(frame)

            mg = np.mean(frame_green)
            mr = np.mean(frame_red)
            mb = np.mean(frame_blue)
            frame = clahe.apply(frame)
            w, h = frame.shape[::-1]

            if mr < 60:
                omitFrame = True
                continue
            else:
                omitFrame = False

            # Apply template Matching

            # make matrix smaller ...
            if len(lastPositions) > 5 and not omitFrame:
                # if we already have a few last positions we can cut down to a smaller map as there is not teleport available:

                n = 250
                p = lastPositions[-1]
                img_small, x1, y1 = submatrix(img, p, n)
                res = cv.matchTemplate(img_small, frame, cv.TM_CCOEFF_NORMED)
                min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
                top_left = (max_loc[0] + x1, max_loc[1] + y1)

                # res = cv.matchTemplate(img, frame, cv.TM_CCOEFF_NORMED)
                # n = 50
                # p = lastPositions[-1]
                # res, x1, y1 = submatrix(res, p, n)
                # min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
                # top_left = (max_loc[0] + x1, max_loc[1] + y1)
            else:
                res = cv.matchTemplate(img, frame, cv.TM_CCOEFF_NORMED)
                min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
                top_left = max_loc

            # check if it is on the island or not ...
            if img_mask[int(top_left[1] + w / 2), int(top_left[0] + h / 2)] < 128:
                omitFrame = True
                # print("frame {:06d} has mask {:03d}".format(count, img_mask[int(top_left[0] + w / 2), int(top_left[1] + h / 2)]))

            # learned in WEKA / trees.J48 -- class 0 means to omitFrame
            # meanred <= 63.814981: 0(611.0 / 5.0)
            # meanred > 63.814981
            # | meanvalue <= 120.041558
            # | | maxval <= 0.75728
            # | | | meanred <= 84.004637
            # | | | | meanblue <= 75.081492: 1(115.0 / 1.0)
            # | | | | meanblue > 75.081492: 0(13.0)
            # | | | meanred > 84.004637
            # | | | | meanblue <= 75.527528: 1(3235.0 / 2.0)
            # | | | | meanblue > 75.527528
            # | | | | | meangreen <= 102.792267: 1(490.0 / 1.0)
            # | | | | | meangreen > 102.792267
            # | | | | | | meanblue <= 116.677664: 1(89.0 / 2.0)
            # | | | | | | meanblue > 116.677664
            # | | | | | | | maxval <= 0.31295: 1(6.0)
            # | | | | | | | maxval > 0.31295: 0(7.0)
            # | | maxval > 0.75728
            # | | | meanblue <= 75.591269: 1(86.0 / 1.0)
            # | | | meanblue > 75.591269: 0(14.0)
            # | meanvalue > 120.041558: 0(334.0)
            # if mr <= 63.814981:
            #     omitFrame = True
            # else:
            #     if lightness <= 120.041558:
            #         if max_val <= 0.75728:
            #             if mr <= 84.004637:
            #                 if mb <= 75.081492: omitFrame = False
            #                 else: omitFrame = True
            #             else:
            #                 if mb <= 75.527528: omitFrame = False
            #                 else:
            #                     if mg <= 102.792267: omitFrame = False
            #                     else:
            #                         if mb <= 116.677664: omitFrame = False
            #                         else:
            #                             if max_val <= 0.31295: omitFrame = False
            #                             else: omitFrame = True
            #         else:
            #              if mb <= 75.591269: omitFrame = False
            #              else: omitFrame = True
            #     else:
            #         omitFrame = True

            if max_val > 0.35 and not omitFrame:
                lastPositions.append(top_left)
            else:  # reset last positions if match is not great
                lastPositions = []

            vis = vis_color.copy()

            if len(lastPositions) > 10:
                lastPositions.reverse()
                lastPositions.pop()
                lastPositions.reverse()

            if len(lastPositions) > 1:
                distance = np.math.sqrt((lastPositions[-2][0] - top_left[0]) ** 2 + (lastPositions[-2][1] - top_left[1]) ** 2)
            else:
                distance = -1
            outfile.write("{:06d} {:4d} {:4d} {:1d} {:1.5f} {:6.3f} {:3f} {:3f} {:3f} {:3f}\n".format(count,
                                                                                                      int(top_left[0] + w / 2),
                                                                                                      int(top_left[1] + h / 2),
                                                                                                      omitFrame,max_val, distance,
                                                                                                      np.mean(frame_red),
                                                                                                      np.mean(frame_green),
                                                                                                      np.mean(frame_blue),
                                                                                                      lightness))
            if count%60 == 0:
                print("{:d} frames processed in {:.2f} seconds, making {:.2f} fps".format(count, time.time() - start_time, count / (time.time() - start_time)))

            if (visualization):
                for pos_idx in range(0, len(lastPositions)):
                    pos = lastPositions[pos_idx]
                    center = (int(pos[0] + w / 2), int(pos[1] + h / 2))
                    cv.circle(vis, center, pos_idx, (0, 0, 255, 50), -1)
                if len(lastPositions) > 0:
                    pos = lastPositions[-1]
                    center = (int(pos[0] + w / 2), int(pos[1] + h / 2))
                    if not omitFrame:
                        heatmap[center[1], center[0]] = heatmap[center[1], center[0]] + 1
                        cv.circle(vis, center, 16, (0, 255, 255, 50), 2)
                    else:
                        cv.circle(vis, center, 16, (255, 0, 255, 50), 3)
                # cv.imshow("result", heatmap*255/np.max(heatmap))
                cv.imshow("result", vis)
                # cv.imshow("result", cv.resize(vis, (0, 0), fx=0.75, fy=0.75))
                cv.imshow("frame", f)
                cv.waitKey(1)
