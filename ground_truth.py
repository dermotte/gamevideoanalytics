import os.path

for count in range(1, 5001):
    isfile = (os.path.isfile('fp/frame_{:d}.png'.format(count)))
    print("{:06d} {:1d}".format(count, not isfile))
