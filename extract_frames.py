import os
import os.path

import shutil

videofiles = """
https://www.youtube.com/watch?v=9zqSV6xmBwY
https://www.youtube.com/watch?v=iKVmajpvrbA
https://www.youtube.com/watch?v=TW147vqcLAA
https://www.youtube.com/watch?v=9PtBkvuN4rM
https://www.youtube.com/watch?v=zy6sw6n_EqI
https://www.youtube.com/watch?v=H3VCrL_pc9c
https://www.youtube.com/watch?v=illGXEyTSfI
https://www.youtube.com/watch?v=2GUXW2V0aLs
https://www.youtube.com/watch?v=gOsIRUqgf9A
https://www.youtube.com/watch?v=pOeyoa9a-hg
https://www.youtube.com/watch?v=_yzKibn2I94
https://www.youtube.com/watch?v=Gfi9F7DYlws
https://www.youtube.com/watch?v=RFYw4a_uD9w
https://www.youtube.com/watch?v=wiAUzhXuU94
https://www.youtube.com/watch?v=Zprxg9VlctY
https://www.youtube.com/watch?v=Ki70pq3w0CI
""".replace("https://www.youtube.com/watch?v=", "").split()


def do(id, crop="185:185:1084:12"):
    if os.path.isdir("frames_" + id):
        shutil.rmtree("frames_" + id)
    os.makedirs("frames_" + id)
    os.system(
        'ffmpeg -i fortnite_' + id + '.mp4 -ss 00:00:01 -r 1 -filter:v "crop='+crop+'" frames_' + id + '/frame_%d.png')


if __name__ == '__main__':
    # do(videofiles[2], "185:185:1076:19")
    # do(videofiles[4], "185:185:1077:18")
    # do(videofiles[5], "185:185:1078:18")
    # do(videofiles[9], "185:185:1077:17")
    do(videofiles[15], "185:185:1017:36")
